package com.example.offline.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.offline.App;
import com.example.offline.domain.GetCategoriesUseCase;
import com.example.offline.domain.GetRankingsUseCase;
import com.example.offline.domain.exception.DefaultErrorBundle;
import com.example.offline.domain.exception.ErrorBundle;
import com.example.offline.model.entity.CategoryInfo;
import com.example.offline.model.DataWrapper;
import com.example.offline.view.exception.ErrorMessageFactory;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subscribers.ResourceSubscriber;
import timber.log.Timber;

public class CategoryViewModel extends ViewModel {


    private final GetCategoriesUseCase getCategoriesUseCase;
    private final GetRankingsUseCase getRankingsUseCase;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private MutableLiveData<DataWrapper<List<CategoryInfo>>> categoryLiveData = new MutableLiveData<>();
    private MutableLiveData<List<String>> rankingLiveData = new MutableLiveData<>();
    private App context;

    public CategoryViewModel(App context, GetCategoriesUseCase getCategoriesUseCase, GetRankingsUseCase getRankingsUseCase) {
        this.getCategoriesUseCase = getCategoriesUseCase;
        this.getRankingsUseCase = getRankingsUseCase;
        this.context = context;
        loadCategories();
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }

    public LiveData<DataWrapper<List<CategoryInfo>>> categories() {
        return categoryLiveData;
    }

    public LiveData<List<String>> rankings() {
        return rankingLiveData;
    }

    void loadCategories() {
        getCategoriesUseCase.getCategories(new CategoryObserver());
    }


    public void refreshRankings() {
        getRankingsUseCase.getRankingName(new RankingObserver());
    }

    private final class RankingObserver implements Consumer<List<String>> {

        @Override
        public void accept(List<String> ranking) throws Exception {
            Timber.i("categoryInfo found", "");
            rankingLiveData.setValue(ranking);
        }
    }

    private final class CategoryObserver extends ResourceSubscriber<List<CategoryInfo>> {
        @Override
        public void onNext(List<CategoryInfo> categoryInfoList) {
            Timber.i("categoryInfo found", "");
            DataWrapper<List<CategoryInfo>> dataWrapper = new DataWrapper<>();
            dataWrapper.setData(categoryInfoList);
            categoryLiveData.setValue(dataWrapper);
        }

        @Override
        public void onError(Throwable e) {
            DataWrapper<List<CategoryInfo>> dataWrapper = new DataWrapper<>();
            dataWrapper.setError(CategoryViewModel.this.getErrorMessage(new DefaultErrorBundle((Exception) e)));
            categoryLiveData.setValue(dataWrapper);
        }

        @Override
        public void onComplete() {

        }

    }

    private String getErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(context,
                errorBundle.getException());
            return errorMessage;
        //this.viewDetailsView.showError(errorMessage);
    }

}
