package com.example.offline.data.datasource;


import com.example.offline.model.ecommodel.EcomData;

import io.reactivex.Observable;

/**
 * Interface that represents a data store from where data is retrieved.
 */
public interface RemoteDataStore {
  Observable<EcomData> getProductData();


}
