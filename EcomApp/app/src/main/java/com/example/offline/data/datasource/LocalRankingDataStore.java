package com.example.offline.data.datasource;

import com.example.offline.data.datasource.dao.ProductRankingDao;
import com.example.offline.model.RankingProductResponse;
import com.example.offline.model.entity.ProductRanking;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Abhishek on 12/3/2017.
 */

public class LocalRankingDataStore implements LocalRankingStore {
    private final ProductRankingDao productRankingDao;
    public LocalRankingDataStore(ProductRankingDao productRankingDao) {
        this.productRankingDao = productRankingDao;
    }
    @Override
    public Flowable<List<String>> getRankingName() {
        return productRankingDao.getProductRankingNameList();
    }

    @Override
    public Flowable<List<Integer>> getProductIdUnderRanking(String ranking) {
        return productRankingDao.getProductRankingId(ranking);
    }

    @Override
    public void clearTable() {
        productRankingDao.clearTable(0);
    }

    @Override
    public void add(List<ProductRanking> productRankings) {
        productRankingDao.add(productRankings);
    }

    @Override
    public Flowable<List<RankingProductResponse>> getProductByRanking(String ranking) {
        return productRankingDao.getProductRankings(ranking);
    }
}
