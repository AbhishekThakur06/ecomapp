package com.example.offline.data.datasource.dao;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.offline.model.entity.CategoryInfo;
import com.example.offline.model.entity.ModelConstants;
import com.example.offline.model.entity.ProductInfo;
import com.example.offline.model.entity.ProductRanking;
import com.example.offline.model.entity.ProductVariant;

@Database(entities = {CategoryInfo.class, ProductRanking.class, ProductInfo.class,
        ProductVariant.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase instance;

    public static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room
                    .databaseBuilder(context.getApplicationContext(), AppDatabase.class, ModelConstants.DB_NAME)
                    .build();
        }
        return instance;
    }

    public abstract CategoryInfoDao categoryInfoDao();

    public abstract ProductInfoDao productInfoDao();

    public abstract ProductVariantDao productVariantDao();

    public abstract ProductRankingDao productRankingDao();

}
