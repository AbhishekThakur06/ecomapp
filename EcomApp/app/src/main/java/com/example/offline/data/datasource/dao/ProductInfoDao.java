package com.example.offline.data.datasource.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.offline.model.entity.ProductInfo;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Abhishek on 11/30/2017.
 */
@Dao
public interface ProductInfoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void add(List<ProductInfo> productInfoList);

    @Query("SELECT * FROM ProductInfo where category_id = :categoryId")
    Flowable<List<ProductInfo>> getProductInfoList(int categoryId);

    @Query("SELECT * FROM ProductInfo where category_id = :categoryId and product_id = :productId ")
    Flowable<ProductInfo> getProduct(int categoryId, int productId);

    @Query("DELETE FROM ProductInfo where id > :idLimit")
    void clearTable(int idLimit);
}