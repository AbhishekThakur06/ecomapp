package com.example.offline.data.datasource;

import com.example.offline.data.datasource.dao.ProductVariantDao;
import com.example.offline.model.entity.ProductVariant;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Abhishek on 12/1/2017.
 */

public class LocalVariantDataStore implements  LocalVariantStore{
    private final ProductVariantDao productVariantDao;
    public LocalVariantDataStore(ProductVariantDao productVariantDao) {
        this.productVariantDao = productVariantDao;
    }
    @Override
    public void putVariants(List<ProductVariant> productVariants) {
         productVariantDao.add(productVariants);
    }

    @Override
    public Flowable<List<ProductVariant>> getProductVariants(int productId) {
        return productVariantDao.getProductVariant(productId);
    }

    @Override
    public void clearTable() {
        productVariantDao.clearTable(0);
    }
}
