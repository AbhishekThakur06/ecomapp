package com.example.offline.di;

import android.content.Context;

import com.example.offline.App;
import com.example.offline.data.datasource.LocalCategoryInfoDataStore;
import com.example.offline.data.datasource.LocalCategoryInfoStore;
import com.example.offline.data.datasource.LocalProductInfoDataStore;
import com.example.offline.data.datasource.LocalProductInfoStore;
import com.example.offline.data.datasource.LocalRankingDataStore;
import com.example.offline.data.datasource.LocalRankingStore;
import com.example.offline.data.datasource.LocalVariantDataStore;
import com.example.offline.data.datasource.LocalVariantStore;
import com.example.offline.data.datasource.dao.AppDatabase;
import com.example.offline.data.datasource.dao.CategoryInfoDao;
import com.example.offline.data.datasource.dao.ProductInfoDao;
import com.example.offline.data.datasource.dao.ProductRankingDao;
import com.example.offline.data.datasource.dao.ProductVariantDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * This is where you will inject application-wide dependencies.
 */
@Module
public class AppModule {

    @Provides
    Context provideContext(App application) {
        return application.getApplicationContext();
    }


    @Singleton
    @Provides
    CategoryInfoDao provideCategoryInfoDao(Context context) {
        return AppDatabase.getInstance(context).categoryInfoDao();
    }

    @Singleton
    @Provides
    ProductInfoDao provideProductInfoDao(Context context) {
        return AppDatabase.getInstance(context).productInfoDao();
    }
    @Singleton
    @Provides
    ProductVariantDao provideProductVariantDao(Context context) {
        return AppDatabase.getInstance(context).productVariantDao();
    }

    @Singleton
    @Provides
    ProductRankingDao provideProductRankingDao(Context context) {
        return AppDatabase.getInstance(context).productRankingDao();
    }
    @Singleton
    @Provides
    LocalCategoryInfoStore provideLocalCategoryInfoRepository(CategoryInfoDao categoryInfoDao) {
        return new LocalCategoryInfoDataStore(categoryInfoDao);
    }

    @Singleton
    @Provides
    LocalProductInfoStore provideLocalProductInfoRepository(ProductInfoDao productInfoDao) {
        return new LocalProductInfoDataStore(productInfoDao);
    }

    @Singleton
    @Provides
    LocalVariantStore provideLocalVariantRepository(ProductVariantDao productVariantDao) {
        return new LocalVariantDataStore(productVariantDao);
    }

    @Singleton
    @Provides
    LocalRankingStore provideLocalRankingRepository(ProductRankingDao productRankingDao) {
        return new LocalRankingDataStore(productRankingDao);
    }


}
