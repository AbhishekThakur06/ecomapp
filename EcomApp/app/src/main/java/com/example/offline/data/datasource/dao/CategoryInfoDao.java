package com.example.offline.data.datasource.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.offline.model.entity.CategoryInfo;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Abhishek on 11/30/2017.
 */
@Dao
public interface CategoryInfoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void add(List<CategoryInfo> categoryInfoList);

    @Query("SELECT * FROM CategoryInfo")
    Flowable<List<CategoryInfo>> getCategoryInfo();

   @Query("DELETE FROM CategoryInfo where category_id > :idLimit")
    void clearTable(int idLimit);
}