package com.example.offline.model;

import android.arch.persistence.room.ColumnInfo;

import com.google.gson.annotations.Expose;

/**
 * Created by Abhishek on 12/4/2017.
 */

public class RankingProductResponse {
    @Expose
    @ColumnInfo(name = "category_id")
    private int categoryId;

    @Expose
    @ColumnInfo(name = "product_id")
    private int productId;

    @Expose
    @ColumnInfo(name = "product_name")
    private String productName;

    @Expose
    @ColumnInfo(name = "date_added")
    private String dateAdded;

    @Expose
    @ColumnInfo(name = "tax_name")
    private String taxName;

    @Expose
    @ColumnInfo(name = "tax_value")
    private float taxValue;

    @Expose
    @ColumnInfo(name = "rank")
    private String rank;

    @Expose
    @ColumnInfo(name = "view_count")
    private int viewCount;


    @Expose
    @ColumnInfo(name = "order_count")
    private int orderCount;


    @Expose
    @ColumnInfo(name = "shares")
    private int shares;


    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getTaxName() {
        return taxName;
    }

    public void setTaxName(String taxName) {
        this.taxName = taxName;
    }

    public float getTaxValue() {
        return taxValue;
    }

    public void setTaxValue(float taxValue) {
        this.taxValue = taxValue;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public int getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(int orderCount) {
        this.orderCount = orderCount;
    }

    public int getShares() {
        return shares;
    }

    public void setShares(int shares) {
        this.shares = shares;
    }
}
