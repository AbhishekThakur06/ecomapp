package com.example.offline.view.fragment;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.offline.R;
import com.example.offline.di.Injectable;
import com.example.offline.model.entity.ProductInfo;
import com.example.offline.model.entity.ProductVariant;
import com.example.offline.viewmodel.AppViewModelFactory;
import com.example.offline.viewmodel.ProductDescriptionViewModel;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by Abhishek on 12/3/2017.
 */

public class ProductDescriptionFragment extends LifecycleFragment implements Injectable {
    private final static String KEY_CATEGORY_ID = "category_id";
    private final static String KEY_PRODUCT_ID = "product_id";
    @Inject
    AppViewModelFactory viewModelFactory;

    @BindView(R.id.product_name)
    TextView productName;

    @BindView(R.id.variants)
    LinearLayout varinatsContainer;
    private ProductDescriptionViewModel viewModel;

    private LifecycleRegistry registry = new LifecycleRegistry(this);

    @Override
    public LifecycleRegistry getLifecycle() {
        return registry;
    }

    private LayoutInflater inflater;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate this data binding layout
        View view = inflater.inflate(R.layout.fragment_product_desc, container, false);
        ButterKnife.bind(this, view);
        this.inflater =inflater;
        // Create and set the adapter for the RecyclerView.
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ProductDescriptionViewModel.class);
        viewModel.setIds(getArguments().getInt(KEY_CATEGORY_ID), getArguments().getInt(KEY_PRODUCT_ID));
        viewModel.comments().observe(this, this :: showProduct );
        viewModel.variants().observe(this,this::showVariant);

    }


    /** Creates project fragment for specific project ID */
    public static ProductDescriptionFragment forProductDescription(int categoryId, int productId) {
        ProductDescriptionFragment fragment = new ProductDescriptionFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_CATEGORY_ID, categoryId);
        args.putInt(KEY_PRODUCT_ID, productId);
        fragment.setArguments(args);

        return fragment;
    }




    private void showVariant(List<ProductVariant> productVariant) {
        Timber.i("product variants");
        varinatsContainer.removeAllViews();
        for(int i = 0; i< productVariant.size(); i++) {
            View v = inflater.inflate(R.layout.variant_item, null, false);
            TextView size = (TextView) v.findViewById(R.id.size);
            TextView color = (TextView) v.findViewById(R.id.color);
            TextView price = (TextView) v.findViewById(R.id.price);
            if(productVariant.get(i).getSize() >0) {
                size.setText(productVariant.get(i).getSize()+"");
            } else {
                size.setVisibility(View.GONE);
            }

            if(productVariant.get(i).getColor() != null) {
                color.setText(productVariant.get(i).getColor()+"");
            } else {
                color.setVisibility(View.GONE);
            }
            if(productVariant.get(i).getPrice() >0) {
                price.setText(productVariant.get(i).getPrice()+"");
            } else {
                price.setVisibility(View.GONE);
            }
            varinatsContainer.addView(v);
        }
    }

    private void showProduct(ProductInfo product) {
        productName.setText(product.getProductName());
    }



}
