package com.example.offline.model.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;

/**
 * Created by Abhishek on 11/30/2017.
 */
@Entity
public class CategoryInfo {
  /*  @Expose
    @PrimaryKey(autoGenerate = true)
    private long id;*/

    @Expose
    @ColumnInfo(name = "category_id")
    @PrimaryKey
    private int categoryId;


    @Expose
    @ColumnInfo(name = "category_name")
    private String categoryName;


    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

 /*   public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }*/
}
