package com.example.offline.data.datasource;

import com.example.offline.model.RankingProductResponse;
import com.example.offline.model.entity.ProductRanking;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Abhishek on 12/3/2017.
 */

public interface LocalRankingStore {
    Flowable<List<String>> getRankingName();
    Flowable<List<Integer>> getProductIdUnderRanking(String ranking);
    void clearTable();
    void add(List<ProductRanking> productRankings);
    Flowable<List<RankingProductResponse>> getProductByRanking(String ranking);
}
