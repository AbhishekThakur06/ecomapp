package com.example.offline.data.datasource;

import android.content.Context;
import android.support.annotation.NonNull;

import com.example.offline.data.mapper.ProductDataJsonMapper;
import com.example.offline.data.net.RestApi;
import com.example.offline.data.net.RestApiImpl;

import javax.inject.Inject;
import javax.inject.Singleton;
/**
 *Factory class to provide all the data stores local as well as cloud
 *
 */

@Singleton
public class AppDataStoreFactory {

    private final Context context;
    private LocalCategoryInfoStore localCategoryInfoStore;
    private LocalVariantStore localVariantStore;
    private LocalProductInfoStore localProductInfoStore;
    private LocalRankingStore localRankingStore;

    @Inject
    AppDataStoreFactory(@NonNull Context context, LocalCategoryInfoStore localCategoryInfoStore,
                        LocalProductInfoStore localProductInfoStore, LocalVariantStore localVariantStore,
                        LocalRankingStore localRankingStore) {
        this.context = context.getApplicationContext();
        this.localCategoryInfoStore = localCategoryInfoStore;
        this.localProductInfoStore = localProductInfoStore;
        this.localVariantStore = localVariantStore;
        this.localRankingStore = localRankingStore;
    }


    /**
     * Create {@link RemoteDataStore} to retrieve data from the Cloud.
     */
    public RemoteDataStore createCloudDataStore() {
        final ProductDataJsonMapper userEntityJsonMapper = new ProductDataJsonMapper();
        final RestApi restApi = new RestApiImpl(this.context, userEntityJsonMapper);
        return new CloudUserDataStore(restApi);
    }

    /**
     *
     * @return to add and retrieve data related to Categories of product
     */
    public LocalCategoryInfoStore getLocalCategoryInfoStore() {
        return localCategoryInfoStore;
    }

    /**
     *
     * @return to add and retrieve data of all the available products
     */
    public LocalProductInfoStore getLocalProductInfoStore() {
        return localProductInfoStore;
    }

    /**
     *
     * @return to add and retrieve data of all the available variant of a specific product
     */
    public LocalVariantStore getLocalProductVariantStore() {
        return localVariantStore;
    }

    /**
     *
     * @return to add and retrieve ids of all the product on the basis of ranking
     */
    public LocalRankingStore getLocalRankingStore() {
        return localRankingStore;
    }
}
