package com.example.offline.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.example.offline.App;
import com.example.offline.domain.GetCategoriesUseCase;
import com.example.offline.domain.GetProductUseCase;
import com.example.offline.domain.GetProductVariantUseCase;
import com.example.offline.domain.GetRankingsUseCase;

public class AppViewModelFactory implements ViewModelProvider.Factory {
    private final GetCategoriesUseCase getCategoriesUseCase;
    private final GetProductUseCase getProductUseCase;
    private final GetProductVariantUseCase getProductVariantUseCase;
    private final GetRankingsUseCase getRankingsUseCase;
    private App context;
    public AppViewModelFactory(App context, GetCategoriesUseCase getCategoriesUseCase,
                               GetProductUseCase getProductUseCase, GetProductVariantUseCase getProductVariantUseCase,
                               GetRankingsUseCase getRankingsUseCase) {
        this.getCategoriesUseCase = getCategoriesUseCase;
        this.getProductUseCase = getProductUseCase;
        this.getProductVariantUseCase = getProductVariantUseCase;
        this.getRankingsUseCase = getRankingsUseCase;
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(CategoryViewModel.class)) {
            return (T) new CategoryViewModel(context, getCategoriesUseCase, getRankingsUseCase);
        } else if(modelClass.isAssignableFrom(ProductListViewModel.class)) {
            return (T) new ProductListViewModel(getProductUseCase, getRankingsUseCase);
        } else if(modelClass.isAssignableFrom(ProductDescriptionViewModel.class)) {
            return (T) new ProductDescriptionViewModel(getProductUseCase, getProductVariantUseCase);
        }
            throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
