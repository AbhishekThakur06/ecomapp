
package com.example.offline.data.mapper;


import com.example.offline.model.ecommodel.EcomData;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import javax.inject.Inject;

/**
 * Class used to transform from Strings representing json to valid objects.
 */
public class ProductDataJsonMapper {

  private final Gson gson;

  @Inject
  public ProductDataJsonMapper() {
    this.gson = new Gson();
  }

  /**
   * Transform from valid json string to {@link EcomData}.
   *
   * @param productListJsonResponse A json representing a collection of categories and the rankings.
   * @return  {@link }.
   * @throws JsonSyntaxException if the json string is not a valid json structure.
   */
  public EcomData transformUserEntityCollection(String productListJsonResponse)
      throws JsonSyntaxException {
    final Type type = new TypeToken<EcomData>() {}.getType();
    return this.gson.fromJson(productListJsonResponse, type);
  }
}
