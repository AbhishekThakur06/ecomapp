package com.example.offline.di;


import com.example.offline.view.fragment.ProductDescriptionFragment;
import com.example.offline.view.fragment.ProductListFragment;
import com.example.offline.view.fragment.CategoryFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuildersModule {
    @ContributesAndroidInjector(modules = CategoryFragmentModule.class)
    abstract CategoryFragment contributeProjectFragment();

    @ContributesAndroidInjector(modules = CategoryFragmentModule.class)
    abstract ProductListFragment contributeProductListFragment();

    @ContributesAndroidInjector(modules = CategoryFragmentModule.class)
    abstract ProductDescriptionFragment contributeProductDescriptionFragment();
}
