package com.example.offline.domain;

import com.example.offline.model.RankingProductResponse;

import java.util.List;

import io.reactivex.functions.Consumer;

/**
 * Created by Abhishek on 12/3/2017.
 */

public class GetRankingsUseCase {
    private final CategoryRepository categoryRepository;

    public GetRankingsUseCase(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public void getRankingName(Consumer<List<String>> consumer) {
        categoryRepository.rankingName(consumer);
    }

    public void getProductByRanking(Consumer<List<RankingProductResponse>> consumer, String rank) {
        categoryRepository.getProductByRank(consumer, rank);
    }
}