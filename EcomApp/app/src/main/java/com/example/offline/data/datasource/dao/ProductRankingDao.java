package com.example.offline.data.datasource.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.offline.model.RankingProductResponse;
import com.example.offline.model.entity.ProductRanking;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Abhishek on 11/30/2017.
 */
@Dao
public interface ProductRankingDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void add(List<ProductRanking> productRankings);

    //@Query("SELECT product_id FROM ProductRanking where rank = :productRankingType")
    @Query("SELECT * FROM ProductInfo INNER JOIN ProductRanking ON ProductInfo.product_id=ProductRanking.product_id where ProductRanking.rank = :productRankingType")
    Flowable<List<RankingProductResponse>> getProductRankings(String productRankingType);

    @Query("SELECT DISTINCT rank FROM ProductRanking")
    Flowable<List<String>> getProductRankingNameList();

    @Query("SELECT product_id FROM ProductRanking where rank = :productRankingType")
    Flowable<List<Integer>> getProductRankingId(String productRankingType);

    @Query("DELETE FROM ProductRanking where id > :idLimit")
    void clearTable(int idLimit);}
