package com.example.offline.model.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;

/**
 * Created by Abhishek on 11/30/2017.
 */
@Entity
public class ProductInfo {

    @Expose
    @PrimaryKey(autoGenerate = true)
    private long id;

    @Expose
    @ColumnInfo(name = "category_id")
    private int categoryId;

    @Expose
    @ColumnInfo(name = "product_id")
    private int productId;

    @Expose
    @ColumnInfo(name = "product_name")
    private String productName;

    @Expose
    @ColumnInfo(name = "date_added")
    private String dateAdded;

    @Expose
    @ColumnInfo(name = "tax_name")
    private String taxName;

    @Expose
    @ColumnInfo(name = "tax_value")
    private float taxValue;

    public long getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getTaxName() {
        return taxName;
    }

    public void setTaxName(String taxName) {
        this.taxName = taxName;
    }

    public float getTaxValue() {
        return taxValue;
    }

    public void setTaxValue(float taxValue) {
        this.taxValue = taxValue;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setId(long id) {
        this.id = id;
    }
}