package com.example.offline.view.adapter;

/**
 * Created by Abhishek on 12/3/2017.
 */

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.offline.R;
import com.example.offline.model.entity.ProductInfo;
import com.example.offline.view.callback.ProductClickCallback;

import java.util.List;

import timber.log.Timber;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {

    private final List<ProductInfo> productInfoDataSet;
    private final ProductClickCallback productClickCallback;
    public ProductListAdapter(List<ProductInfo> productInfoDataSet, ProductClickCallback productClickCallback) {
        this.productInfoDataSet = productInfoDataSet;
        this.productClickCallback = productClickCallback;
    }

    @Override
    public ProductListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        TextView commentText = (TextView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_row, parent, false);
        return new ProductListAdapter.ViewHolder(commentText);
    }

    @Override
    public void onBindViewHolder(ProductListAdapter.ViewHolder holder, int position) {
        final ProductInfo productInfo = productInfoDataSet.get(position);
        /*if (comment.isSyncPending()) {
            holder.commentText.setTextColor(Color.LTGRAY);
        } else {
        */    holder.commentText.setTextColor(Color.BLACK);
        //}
        holder.commentText.setText(productInfo.getProductName());
        holder.commentText.setOnClickListener(new OnProductClickListener(productInfoDataSet.get(position)));
    }

    @Override
    public int getItemCount() {
        return productInfoDataSet == null ? 0 : productInfoDataSet.size();
    }

    public void updateCommentList(List<ProductInfo> products) {
        Timber.d("Got new product " + products.size());
        this.productInfoDataSet.clear();
        this.productInfoDataSet.addAll(products);
        notifyDataSetChanged();
    }

    /**
     * View holder for shopping list items of this adapter
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView commentText;

        public ViewHolder(final TextView commentText) {
            super(commentText);
            this.commentText = commentText;
        }
    }

    class OnProductClickListener implements View.OnClickListener {
        ProductInfo productInfo;
        OnProductClickListener(ProductInfo productInfo) {
            this.productInfo = productInfo;
        }
        @Override
        public void onClick(View view) {
            productClickCallback.onClick(productInfo);
        }
    };
}
