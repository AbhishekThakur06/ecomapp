package com.example.offline.di;

import com.example.offline.App;
import com.example.offline.data.CategoriesDataRepository;
import com.example.offline.domain.GetCategoriesUseCase;
import com.example.offline.domain.GetProductUseCase;
import com.example.offline.domain.GetProductVariantUseCase;
import com.example.offline.domain.GetRankingsUseCase;
import com.example.offline.viewmodel.AppViewModelFactory;

import dagger.Module;
import dagger.Provides;

/**
 * Define CommentsActivity-specific dependencies here.
 */
@Module
public class CategoryFragmentModule {
    @Provides
    AppViewModelFactory provideCommentsViewModelFactory(App context, GetCategoriesUseCase getCategoriesUseCase,
                                                        GetProductUseCase getProductUseCase,
                                                        GetProductVariantUseCase getProductVariantUseCase,
                                                        GetRankingsUseCase getRankingsUseCase) {
        return new AppViewModelFactory(context,getCategoriesUseCase,
                getProductUseCase, getProductVariantUseCase, getRankingsUseCase);
    }


    @Provides
    GetCategoriesUseCase provideGetCategoriesUseCase(CategoriesDataRepository categoriesDataRepository) {
        return new GetCategoriesUseCase(categoriesDataRepository);
    }

    @Provides
    GetProductUseCase provideGetProductUseCase(CategoriesDataRepository categoriesDataRepository) {
        return new GetProductUseCase(categoriesDataRepository);
    }

    @Provides
    GetProductVariantUseCase provideGetProductVariantUseCase(CategoriesDataRepository categoriesDataRepository) {
        return new GetProductVariantUseCase(categoriesDataRepository);
    }

    @Provides
    GetRankingsUseCase provideGetRankingUseCase(CategoriesDataRepository categoriesDataRepository) {
        return new GetRankingsUseCase(categoriesDataRepository);
    }
}
