package com.example.offline.view.fragment;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.offline.R;
import com.example.offline.di.Injectable;
import com.example.offline.model.RankingProductResponse;
import com.example.offline.model.entity.ProductInfo;
import com.example.offline.view.MainActivity;
import com.example.offline.view.adapter.ProductListAdapter;
import com.example.offline.view.adapter.RankedProductListAdapter;
import com.example.offline.view.callback.ProductClickCallback;
import com.example.offline.view.callback.RankedProductClickCallback;
import com.example.offline.viewmodel.AppViewModelFactory;
import com.example.offline.viewmodel.ProductListViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Abhishek on 12/3/2017.
 */
public class ProductListFragment extends LifecycleFragment implements Injectable {
    private final static String KEY_CATEGORY_ID = "category_id";
    private final static String KEY_RANKING_ID = "ranking_id";
    @Inject
    AppViewModelFactory viewModelFactory;

    @BindView(R.id.comments_recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.caption)
    TextView caption;
    private ProductListAdapter recyclerViewAdapter;
    private RankedProductListAdapter recyclerViewRankedAdapter;
    private ProductListViewModel viewModel;

    private LifecycleRegistry registry = new LifecycleRegistry(this);

    @Override
    public LifecycleRegistry getLifecycle() {
        return registry;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate this data binding layout
        View view = inflater.inflate(R.layout.fragment_product_listing, container, false);
        ButterKnife.bind(this, view);

        initRecyclerView();

        // Create and set the adapter for the RecyclerView.
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ProductListViewModel.class);
        viewModel.setCategoryId(getArguments().getInt(KEY_CATEGORY_ID), getArguments().getString(KEY_RANKING_ID));
       if(getArguments().getString(KEY_RANKING_ID) != null) {
           caption.setText(getArguments().getString(KEY_RANKING_ID));
       }
        viewModel.products().observe(this, this::handleProducts );
        viewModel.rankedProducts().observe(this, this::handleRankedProducts);
        progressBar.setVisibility(View.VISIBLE);
    }


    /** Creates ProductListFragment fragment for specific categoryId ID */
    public static ProductListFragment forProduct(int categoryId, String rankingId ) {
        ProductListFragment fragment = new ProductListFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_CATEGORY_ID, categoryId);
        args.putString(KEY_RANKING_ID, rankingId);
        fragment.setArguments(args);

        return fragment;
    }

    private void initRecyclerView() {
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager recyclerViewLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(recyclerViewLayoutManager);

        recyclerViewAdapter = new ProductListAdapter(new ArrayList<>(), productClickCallback);
        recyclerView.setAdapter(recyclerViewAdapter);
    }


    private final ProductClickCallback productClickCallback = new ProductClickCallback() {
        @Override
        public void onClick(ProductInfo info) {
            if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
           ((MainActivity) getActivity()).showProductDescriptionFragment(viewModel.getCategoryId(), info.getProductId());
            }
        }
    };

    private void handleRankedProducts(List<RankingProductResponse> rankingProductResponses) {
        progressBar.setVisibility(View.GONE);
        initRecyclerViewWithRankedProducts();
        recyclerViewRankedAdapter.updateCommentList(rankingProductResponses);
    }

    private void handleProducts(List<ProductInfo> products) {
        progressBar.setVisibility(View.GONE);
        recyclerViewAdapter.updateCommentList(products);
    }

    private void initRecyclerViewWithRankedProducts() {
        if (recyclerViewRankedAdapter == null) {
            recyclerView.setHasFixedSize(true);

            RecyclerView.LayoutManager recyclerViewLayoutManager = new LinearLayoutManager(getContext());
            recyclerView.setLayoutManager(recyclerViewLayoutManager);

            recyclerViewRankedAdapter = new RankedProductListAdapter(new ArrayList<>(), rankedProductClickCallback);
            recyclerView.setAdapter(recyclerViewRankedAdapter);
        }
    }

    private final RankedProductClickCallback rankedProductClickCallback = new RankedProductClickCallback() {
        @Override
        public void onClick(RankingProductResponse info) {
            if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
                ((MainActivity) getActivity()).showProductDescriptionFragment(info.getCategoryId(), info.getProductId());
            }
        }
    };
}
