package com.example.offline.view;

import android.arch.lifecycle.LifecycleActivity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.example.offline.R;
import com.example.offline.view.fragment.ProductDescriptionFragment;
import com.example.offline.view.fragment.ProductListFragment;
import com.example.offline.view.fragment.CategoryFragment;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class MainActivity extends LifecycleActivity implements HasSupportFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        show();
    }

    /** Shows the CategoryFragment to show all the available category */
    public void show() {
        CategoryFragment projectFragment = CategoryFragment.forProject("project");
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("project")
                .replace(R.id.fragment_container,
                        projectFragment, null).commit();
    }

    public void showProductFragment(int id, String rank) {
        ProductListFragment productListFragment = ProductListFragment.forProduct(id, rank);

        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("product")
                .replace(R.id.fragment_container,
                        productListFragment, null).commit();
    }

    public void showProductDescriptionFragment(int categoryId, int productId) {
        ProductDescriptionFragment productDescriptionFragment = ProductDescriptionFragment.forProductDescription(categoryId, productId);
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("product_desc")
                .replace(R.id.fragment_container,
                        productDescriptionFragment, null).commit();
    }
    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }
}
