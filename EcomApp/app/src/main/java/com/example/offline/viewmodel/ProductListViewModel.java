package com.example.offline.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.offline.domain.GetProductUseCase;
import com.example.offline.domain.GetRankingsUseCase;
import com.example.offline.model.RankingProductResponse;
import com.example.offline.model.entity.ProductInfo;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import timber.log.Timber;

/**
 * Created by Abhishek on 12/3/2017.
 */

public class ProductListViewModel extends ViewModel {


    private final GetProductUseCase getProductUseCase;
    private  final GetRankingsUseCase getRankingsUseCase;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private MutableLiveData<List<ProductInfo>> productsLiveData = new MutableLiveData<>();
    private MutableLiveData<List<RankingProductResponse>> rankingProductLiveData = new MutableLiveData<>();
    private int categoryId;
    String rankingId;
    public ProductListViewModel(GetProductUseCase getProductUseCase, GetRankingsUseCase getRankingsUseCase) {
        this.getProductUseCase = getProductUseCase;
        this.getRankingsUseCase = getRankingsUseCase;

    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }


    public LiveData<List<ProductInfo>> products() {
        return productsLiveData;
    }

    public LiveData<List<RankingProductResponse>> rankedProducts() {
        return rankingProductLiveData;
    }
    void loadProducts() {
        getProductUseCase.getProducts(new ProductObserver(), categoryId);
    }


    private final class ProductObserver implements Consumer<List<ProductInfo>> {

        @Override
        public void accept(List<ProductInfo> productInfoList) throws Exception {
            Timber.i("categoryInfo found","");
            productsLiveData.setValue(productInfoList);
        }
    }

    public void setCategoryId(int categoryId, String rankingId) {
        this.categoryId = categoryId;
        this.rankingId = rankingId;
        if(rankingId != null) {
         loadProductsWithRanking();
        } else {
            loadProducts();
        }
    }

    public int getCategoryId() {
        return this.categoryId;
    }

    void loadProductsWithRanking() {
        getRankingsUseCase.getProductByRanking(new RankedProductObserver(), rankingId);
    }

    private final class RankedProductObserver implements Consumer<List<RankingProductResponse>> {

        @Override
        public void accept(List<RankingProductResponse> productInfoList) throws Exception {
            Timber.i("categoryInfo found","");
            rankingProductLiveData.setValue(productInfoList);
        }
    }
}
