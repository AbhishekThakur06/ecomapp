package com.example.offline.view.callback;

import com.example.offline.model.RankingProductResponse;

/**
 * Created by Abhishek on 12/4/2017.
 */

public interface RankedProductClickCallback {
    public void onClick(RankingProductResponse productInfo);
}
