package com.example.offline.data.datasource;

import com.example.offline.model.entity.ProductInfo;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Abhishek on 12/1/2017.
 */

public interface LocalProductInfoStore {

    void putProducts(List<ProductInfo> productInfoList);
    Flowable<List<ProductInfo>> getProducts(int categoryId);
    Flowable<ProductInfo> getProduct(int categoryId, int productId);
    void clearTable();
}
