package com.example.offline.di;


import com.example.offline.viewmodel.ProductListViewModel;
import dagger.Subcomponent;

/**
 * A sub component to create ViewModels. It is called by the
 */
@Subcomponent
public interface ViewModelSubComponent {
    @Subcomponent.Builder
    interface Builder {
        ViewModelSubComponent build();
    }

    ProductListViewModel productListViewModel();
}
