
package com.example.offline.data.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.offline.data.exception.NetworkConnectionException;
import com.example.offline.data.mapper.ProductDataJsonMapper;
import com.example.offline.model.ecommodel.EcomData;

import java.net.MalformedURLException;

import io.reactivex.Observable;


/**
 * {@link RestApi} implementation for retrieving data from the network.
 */
public class RestApiImpl implements RestApi {

  private final Context context;
  private final ProductDataJsonMapper userEntityJsonMapper;

  /**
   * Constructor of the class
   *
   * @param context {@link Context}.
   * @param userEntityJsonMapper {@link ProductDataJsonMapper}.
   */
  public RestApiImpl(Context context, ProductDataJsonMapper userEntityJsonMapper) {
    if (context == null || userEntityJsonMapper == null) {
      throw new IllegalArgumentException("The constructor parameters cannot be null!!!");
    }
    this.context = context.getApplicationContext();
    this.userEntityJsonMapper = userEntityJsonMapper;
  }

  @Override public Observable<EcomData> ecomData() {
    return Observable.create(emitter -> {
      if (isThereInternetConnection()) {
        try {
          String responseUserEntities = getEcomDataFromApi();
          if (responseUserEntities != null) {
            emitter.onNext(userEntityJsonMapper.transformUserEntityCollection(
                responseUserEntities));
            emitter.onComplete();
          } else {
            emitter.onError(new NetworkConnectionException());
          }
        } catch (Exception e) {
          emitter.onError(new NetworkConnectionException(e.getCause()));
        }
      } else {
        emitter.onError(new NetworkConnectionException());
      }
    });
  }

  private String getEcomDataFromApi() throws MalformedURLException {
    return ApiConnection.createGET(API_URL_GET_CATEGORY_LIST).requestSyncCall();
  }

  /**
   * Checks if the device has any active internet connection.
   *
   * @return true device with internet connection, otherwise false.
   */
  private boolean isThereInternetConnection() {
    boolean isConnected;

    ConnectivityManager connectivityManager =
        (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
    isConnected = (networkInfo != null && networkInfo.isConnectedOrConnecting());

    return isConnected;
  }
}
